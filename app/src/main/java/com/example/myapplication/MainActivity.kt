package com.example.myapplication

import android.app.Activity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import com.et.cinemax.Repo.UserRepository
import com.example.myapplication.data.database
import com.example.myapplication.repo.ItemRepository

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.one_item_view.view.*
import kotlinx.android.synthetic.main.one_my_item_view.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        setSupportActionBar(toolbar)

    }

    override fun onSupportNavigateUp() = findNavController(this, R.id.main_frame).navigateUp()

    fun ToDetail(view: View){
        val SelectedID=view.itemid.text.toString()
        Toast.makeText(this,SelectedID,Toast.LENGTH_LONG).show()
        var bundle: Bundle= Bundle()
        bundle.putString("itemID",SelectedID)
        val navController1 = findNavController(this,R.id.main_frame)
        navController1.navigate(R.id.action_itemList_to_itemDetailFragment,bundle)
    }
    fun toManage(view: View){
        val navController1 = findNavController(this,R.id.main_frame)
        navController1.navigate(R.id.action_itemList_to_myItems,null)
    }
    fun toAddItem(view: View){
        val navController1 = findNavController(this,R.id.main_frame)
        navController1.navigate(R.id.action_myItems_to_addItem,null)
    }
    fun deleteMine(view: View){
        val sel=view.one_item_id.text.toString()
        Toast.makeText(this,sel,Toast.LENGTH_LONG).show()
        val itemDao = database.getDatabase(this).itemDao()
        val itemRepo = ItemRepository(itemDao)
        GlobalScope.launch {
        val del=itemRepo.delete(sel.toInt())
        val resp=del.execute().body()
        if (resp!=null){
            if(resp.equals("1")){
                runOnUiThread {
                Toast.makeText(applicationContext,"Item Deleted",Toast.LENGTH_LONG).show()
                }
            }
            else{
                runOnUiThread{
                Toast.makeText(applicationContext,"Cannot Delete",Toast.LENGTH_LONG).show()
                }
            }
        }
        }

    }
}
