package com.example.myapplication

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_items.*
import kotlinx.android.synthetic.main.fragment_items.view.*
import kotlin.text.category

class ItemsFragment : Fragment() {
    private val array = ArrayList<Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = LayoutInflater.from(container?.context).inflate(R.layout.fragment_items, container, false)

        val array = ArrayList<String>()

        array.add("fdsd")

        for(x in 0..10) {
            this.array.add(Item("dfd" + x, "ewsdd" + x, "sdsadas" + x, "dfsadas" + x, 5, "sds", "sdsdsa", "fsdf", 42121, "fdssadf sdf dsf ds fdsf dsf ", "sdfdsf", "fdsgdf", "gdsfgd", "dfsgdsf", "dfsds", true, array))
        }
        val adapter = ItemListAdapter(container!!.context, this.array)

        v.items.layoutManager = LinearLayoutManager(container!!.context)
        v.items.adapter = adapter

        return  v
    }

}
