package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.et.cinemax.Repo.UserRepository
import kotlinx.android.synthetic.main.fragment_register.*
import com.example.myapplication.data.User
import com.example.myapplication.data.database
import com.example.myapplication.network.ApiService
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_register.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [RegisterFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class RegisterFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val v: View = inflater.inflate(R.layout.fragment_register, container, false)

        v.register_sign_up_btn.setOnClickListener {
            //Toast.makeText(activity,"Registering",Toast.LENGTH_LONG).show()

            val userDao = database.getDatabase(activity!!).userDao()
            val userRepo = UserRepository(userDao)
            val username = register_username.text.toString()
            val password = register_password.text.toString()
            val confirm_password = register_confirm_password.text.toString()
            val full_name = register_full_name.text.toString()
            val phone = register_phone.text.toString()
            GlobalScope.launch {
            val user=User(0,"",full_name,phone ,"",password,username)
                val userLog = userRepo.register(user)
                try {
                val registerResponse=userLog.execute().body()
                    if (registerResponse.equals("1")){
                        activity!!.runOnUiThread {
                        Toast.makeText(activity,"Succesfully SignedUp",Toast.LENGTH_LONG).show()
                        }
                    }
                    else{
                        activity!!.runOnUiThread {
                            Toast.makeText(activity,"Error While registering",Toast.LENGTH_LONG).show()
                        }
                    }
                }
                catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }
        return v
    }

}
