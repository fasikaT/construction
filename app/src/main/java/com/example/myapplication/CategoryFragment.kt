package com.example.myapplication

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_category.view.*
import kotlinx.android.synthetic.main.fragment_items.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.text.category

class CategoryFragment : Fragment() {
    private val array = ArrayList<Category>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_category, container, false)

        val list = ArrayList<Category>()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://10.0.2.2:3000/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()


        val api = retrofit.create(ApiService::class.java)

        api.fetchAllCategories().enqueue(object : Callback<ArrayList<Category>>{

            override fun onResponse(call: Call<ArrayList<Category>>, response: Response<ArrayList<Category>>) {

                val data = response.body()!!

                val adapter = CategoryListAdapter(container!!.context, data)

                v.category.layoutManager = LinearLayoutManager(container.context)
                v.category.adapter = adapter

            }

            override fun onFailure(call: Call<ArrayList<Category>>, t: Throwable) {

//                val adapter = CategoryListAdapter(container!!.context, List<Category>[])

                v.category.layoutManager = LinearLayoutManager(container!!.context)
//                v.category.adapter = adapter
                println(t.message)
            }

        })

        val adapter = CategoryListAdapter(container!!.context, list)

        v.category.layoutManager = LinearLayoutManager(container.context)
        v.category.adapter = adapter

        return  v
    }

}
