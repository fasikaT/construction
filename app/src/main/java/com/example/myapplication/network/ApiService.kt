
package com.example.myapplication.network
import com.example.myapplication.data.Item
import com.example.myapplication.data.User
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface ApiService {
    @GET("items")
    fun fetchAllItems(): Call<List<Item>>

    @POST("users/login")
    fun login(@Body user:User): Call<String>

    @POST("users/register")
    fun register(@Body user : User): Call<String>

    @GET("items/{uid}")
    fun fetchMyItems(@Path("uid") uid:Int): Call<List<Item>>

    @POST("items/add")
    fun addItem(@Body item:Item):Call<String>

    @GET("users/{id}")
    fun getInfoFromNet(@Path("id") uid:Int) :Call<User>

    @GET("items/delete/{itid}")
    fun delete(@Path("itid") id: Int): Call<String>

    companion object {

        private val baseUrl = "http://192.168.43.19:30/controller/"

        fun getInstance(): ApiService {
            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}
