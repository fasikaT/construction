package com.example.myapplication


import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.et.cinemax.Repo.UserRepository
import com.example.myapplication.data.database
import com.example.myapplication.databinding.FragmentItemDetailBinding
import com.example.myapplication.repo.ItemRepository
import com.example.myapplication.viewModel.ItemViewModel
import com.example.myapplication.viewModel.UserViewModel
import com.squareup.picasso.Picasso
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ItemDetailFragment : Fragment() {

    lateinit var itemViewModelx: ItemViewModel
    lateinit var userViewModel:UserViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bindDetail= DataBindingUtil.inflate<FragmentItemDetailBinding>(inflater, R.layout.fragment_item_detail,container, false)
        bindDetail.setLifecycleOwner(this.activity)
        val idS=arguments?.getString("itemID")
        val intID=if (idS!=null) idS.toInt() else 0
        var userId:Int=0
        val itemDao= database.getDatabase(activity!!).itemDao()
        val itemRepo= ItemRepository(itemDao)
        itemViewModelx= ViewModelProviders.of(this).get(ItemViewModel::class.java)
        itemViewModelx.getOne(intID).observe(this, Observer {
            it?.let {
                bindDetail.oneItemBinding=it!!
                userId=it.postinguserid
                Picasso.get()
                    .load("https://picsum.photos/id/0/200")
                    .fit()
                    .into(bindDetail.imageViewDetail)
            }
        })
        GlobalScope.launch {
            val userDao= database.getDatabase(activity!!).userDao()
            val userRepo= UserRepository(userDao)
            val userResp=userRepo.getInfoFromNet(userId)
            val fResp=userResp.execute().body()
            if(fResp!=null){
                bindDetail.userDetailBinding=fResp
            }
        }

        bindDetail.contactuser.setOnClickListener {
            GlobalScope.launch {
                val userDao= database.getDatabase(activity!!).userDao()
                val userRepo= UserRepository(userDao)
                val userResp=userRepo.getInfoFromNet(userId)
                val fResp=userResp.execute().body()
                if(fResp!=null){
                    val builder = AlertDialog.Builder(activity)
                    activity!!.runOnUiThread {
                        builder.setTitle("Phone Number-"+fResp.phone)
                        builder.setMessage("Name-"+fResp.fullName)
                        builder.setPositiveButton("Ok"){dialog, which ->
                        }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()
                    }
                }
            }

        }
        return bindDetail.root
    }


}
