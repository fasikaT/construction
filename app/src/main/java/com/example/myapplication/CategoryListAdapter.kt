package com.example.myapplication

import android.content.ClipDescription
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.myapplication.data.Item
import kotlinx.android.synthetic.main.category_list.view.*
import kotlinx.android.synthetic.main.fragment_category.view.*
import java.time.Duration
import kotlin.text.category

class CategoryListAdapter(var context: Context, var list: List<Item>): androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val v: View = LayoutInflater.from(context).inflate(R.layout.category_list, p0, false)
        return CategoryHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: androidx.recyclerview.widget.RecyclerView.ViewHolder, p1: Int) {
        (p0 as CategoryHolder).bind(list[p1].name)
    }

}

class CategoryHolder(itemView: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    fun bind(name: String) {
        itemView.category_name.text = name

        itemView.singleCategory.setOnClickListener {
            Toast.makeText(itemView.context, "clicked", Toast.LENGTH_LONG).show()

        }
    }
}