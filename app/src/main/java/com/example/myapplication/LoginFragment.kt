package com.example.myapplication

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.et.cinemax.Repo.UserRepository
import com.example.myapplication.data.User
import com.example.myapplication.data.database
import com.example.myapplication.network.ApiService
import com.example.myapplication.repo.ItemRepository
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class LoginFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_login, container, false)


        v.login_btn.setOnClickListener {
            val username = v.login_username.text.toString()
            val password = v.login_password.text.toString()
            val user = User(0, "", "", "", "", password, username)
            val userDao = database.getDatabase(activity!!).userDao()
            val userRepo = UserRepository(userDao)
            val userLog = userRepo.login(user)
            val itemDao = database.getDatabase(activity!!).itemDao()
            val itemRepo = ItemRepository(itemDao)
            GlobalScope.launch {
                try{
                    var responded=userLog.execute().body()
                    //var responded=1
                    if (responded.equals("0")){
                        activity!!.runOnUiThread {
                            Toast.makeText(activity,"Incorrect Credential...",Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        activity!!.runOnUiThread {
                            Toast.makeText(activity, responded.toString(), Toast.LENGTH_SHORT).show()
                        }
                        userRepo.DeleteAll()
                        userRepo.Save(User(Integer.parseInt(responded), "", "", "", "", password, username))
                        itemRepo.clear()
                        activity!!.runOnUiThread{
                            val navController = Navigation.findNavController(activity!!, R.id.main_frame)
                            navController.navigate(R.id.action_loginFragment_to_itemList,null,
                                NavOptions.Builder()
                                    .setPopUpTo(R.id.loginFragment, true).build())
                        }
                    }
                }
                catch(ex:Exception) {
                    ex.printStackTrace()
                    activity!!.runOnUiThread {
                        Toast.makeText(activity,"Connection Error...",Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
        v.login_sign_up_btn.setOnClickListener {
            activity!!.runOnUiThread{
                val navController = Navigation.findNavController(activity!!, R.id.main_frame)
                navController.navigate(R.id.action_loginFragment_to_register)
            }
        }
        return v
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
}
