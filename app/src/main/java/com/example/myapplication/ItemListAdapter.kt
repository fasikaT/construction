package com.example.myapplication

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.data.Item
import kotlinx.android.synthetic.main.category_list.view.*
import kotlinx.android.synthetic.main.one_item_view.view.*

class ItemListAdapter(var context: Context, var list: ArrayList<Item>): androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val v: View = LayoutInflater.from(context).inflate(R.layout.fragment_item_list, p0, false)
        return ItemHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: androidx.recyclerview.widget.RecyclerView.ViewHolder, p1: Int) {
        (p0 as ItemHolder).bind(list[p1].type, emptyList<String>(),list[p1].price,"" )
    }
}

class ItemHolder(itemView: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    fun bind(type: String, photo: List<String>, price: Int, forr: String) {
      //  itemView.one_item_type.text = type
        //itemView.one_item_price.text = price.toString()
        //itemView.one_item_name.text = forr
    }
}