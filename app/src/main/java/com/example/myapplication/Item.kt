package com.example.myapplication

class Item {
    var id: String
    var type: String
    var make: String
    var rating: String
    var timestamp: Int
    var forr: String
    var model: String
    var city: String
    var price: Int
    var description: String
    var actas: String
    var phone: String
    var userId: String
    var storeId: String
    var categoryId: String
    var verified: Boolean
    var images: ArrayList<String>

    constructor(
        id: String,
        type: String,
        make: String,
        rating: String,
        timestamp: Int,
        forr: String,
        model: String,
        city: String,
        price: Int,
        description: String,
        actas: String,
        phone: String,
        userId: String,
        storeId: String,
        categoryId: String,
        verified: Boolean,
        images: ArrayList<String>) {

        this.id = id
        this.type = type
        this.make = make
        this.rating = rating
        this.timestamp = timestamp
        this.forr = forr
        this.model = model
        this.city = city
        this.price = price
        this.description = description
        this.actas = actas
        this.phone = phone
        this.userId = userId
        this.storeId = storeId
        this.categoryId = categoryId
        this.verified = verified
        this.images = images
    }
}