package com.example.myapplication.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.myapplication.data.Item
import com.example.myapplication.data.ItemDao
import com.example.myapplication.data.User
import com.example.myapplication.data.database
import com.example.myapplication.repo.ItemRepository

class ItemViewModel(application: Application) : AndroidViewModel(application) {

    var itemda: ItemDao
    var itemRepo: ItemRepository
    val itemList: LiveData<List<Item>>

    init {
        itemda= database.getDatabase(application).itemDao()
        itemRepo= ItemRepository(itemda)
        itemList= itemRepo.readFromDb()
    }

    fun save(item: Item){
        itemRepo.saveToDb(item)
    }
    fun getOne(id:Int):LiveData<Item>{
        return itemRepo.getOne(id)
    }
    fun getMyItemsList(id:Int):LiveData<List<Item>>{
        return itemRepo.getMyItemsFromDb(id)
    }

}