package com.example.myapplication


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.et.cinemax.Repo.UserRepository
import com.example.myapplication.data.Item
import com.example.myapplication.data.database
import com.example.myapplication.repo.ItemRepository
import com.example.myapplication.viewModel.ItemViewModel
import kotlinx.android.synthetic.main.fragment_item_list.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ItemList : Fragment() {
    lateinit var recyclervT: RecyclerView
    lateinit var itemViewModel: ItemViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view= inflater.inflate(R.layout.fragment_item_list, container, false)
        view.progressBar.setVisibility(View.VISIBLE)
        GlobalScope.launch {
            val itemDao = database.getDatabase(activity!!).itemDao()
            val itemRepo = ItemRepository(itemDao)
            val getItem = itemRepo.getItemList()
            var fetchedItems:List<Item>
            try {
                fetchedItems =getItem.execute().body()!!
                if(fetchedItems!=null){
                    for(item in fetchedItems)
                        itemRepo.saveToDb(item)
                }
                activity!!.runOnUiThread {
                    try{
                    view.progressBar.setVisibility(View.GONE)
                    }
                    catch(ex:Exception){

                    }
                }
            }
            catch (ex:Exception){
                activity!!.runOnUiThread {
                    Toast.makeText(activity,"Connection Error...", Toast.LENGTH_SHORT).show()
                    view.progressBar.setVisibility(View.GONE)
                }
            }
            activity!!.runOnUiThread {
                itemViewModel = ViewModelProviders.of(this@ItemList).get(ItemViewModel::class.java)
                val ticketsadapter =ItemAdapter(this@ItemList.context!!)
                itemViewModel.itemList.observe(this@ItemList, Observer {
                    ticketsadapter.setItemList(it)})
                recyclervT=view.items
                recyclervT.layoutManager= LinearLayoutManager(this@ItemList.context)
                val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
                recyclervT.setLayoutAnimation(controller)
                recyclervT.adapter=ticketsadapter
            }
        }

        return view
    }


}
