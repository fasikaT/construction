package com.example.myapplication.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserDao {
    @Query("Select * from user")
    fun getUserFromDb():LiveData<User>

    @Query("Select * from user")
    fun DirectgetUserFromDb(): User

    @Query("Delete from USER ")
    fun ClearAllUser()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(user: User):Long
}