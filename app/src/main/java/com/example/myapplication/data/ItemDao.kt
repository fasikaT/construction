package com.example.myapplication.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface ItemDao {

    @Query("select * from item where postingUserID=:id")
    fun getMyItemtest(id: Int):List<Item>

    @Query("select * from item where postingUserID=:id")
    fun getMyItemFromDb(id: Int):LiveData<List<Item>>

    @Query("Select * from item")
    fun getAllFromDb(): LiveData<List<Item>>

    @Query("Delete from item")
    fun ClearAllItems()

    @Query("select * from item where id=:id")
    fun getOne(id: Int):LiveData<Item>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(item: Item):Long
}