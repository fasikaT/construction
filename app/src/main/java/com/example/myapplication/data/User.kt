package com.example.myapplication.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class User(@PrimaryKey val id: Int,
    val email: String,
    val fullName: String,
    val phone: String,
    val userType: String,
    val password: String,
    val username: String
)