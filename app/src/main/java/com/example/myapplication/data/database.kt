package com.example.myapplication.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Item::class,User::class),version=7)
abstract class database: RoomDatabase() {
    abstract fun itemDao():ItemDao
    abstract fun userDao():UserDao

    companion object {
        @Volatile
        private var INSTANCE :database?=null

        fun getDatabase(context: Context):database{
            val tempInstance= INSTANCE
            if(tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val instance= Room.databaseBuilder(
                    context.applicationContext,
                    database::class.java,"database")
                    .fallbackToDestructiveMigration()
                    .build()
                return instance
            }
        }
    }
}