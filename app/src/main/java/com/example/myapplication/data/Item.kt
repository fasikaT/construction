package com.example.myapplication.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Item(@PrimaryKey var id: Int,
    var type: String,
    var price: Int,
    var description: String,
    val name: String,
    val postinguserid:Int
)