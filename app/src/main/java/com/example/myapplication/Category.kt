package com.example.myapplication

class Category {
    var id: String
    var name: String

    constructor(id: String, name: String) {
        this.id = id
        this.name = name
    }
}

//package com.example.myapplication
//
//data class Category(
//    val id: String,
//    val name: String
//)