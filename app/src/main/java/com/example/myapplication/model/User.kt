package com.example.myapplication.model

data class User(
    val email: String,
    val fullName: String,
    val id: String,
    val phone: String,
    val userType: String,
    val password: String,
    val username: String
)