package com.example.myapplication.model

data class Category(
    val id: String,
    val name: String
)