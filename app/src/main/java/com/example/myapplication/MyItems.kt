package com.example.myapplication


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.et.cinemax.Repo.UserRepository
import com.example.myapplication.data.Item
import com.example.myapplication.data.database
import com.example.myapplication.repo.ItemRepository
import com.example.myapplication.viewModel.ItemViewModel
import kotlinx.android.synthetic.main.fragment_item_list.view.*
import kotlinx.android.synthetic.main.fragment_my_items.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MyItems : Fragment() {
    lateinit var recyclervTM: RecyclerView
    lateinit var itemViewModelM: ItemViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_my_items, container, false)
        //view.progressBar.setVisibility(View.VISIBLE)
        GlobalScope.launch {
            val userDao= database.getDatabase(activity!!).userDao()
            val itemDao = database.getDatabase(activity!!).itemDao()
            val userRepo=UserRepository(userDao)
            val user=userRepo.DirectRepoGetUserFromDatabase()
            val itemRepo = ItemRepository(itemDao)
            val getItem = itemRepo.getMyItems(user.id)
            var fetchedItems:List<Item>
            itemRepo.clear()
            try {
                fetchedItems =getItem.execute().body()!!

                if(fetchedItems!=null){
                    for(item in fetchedItems){
                            itemRepo.saveToDb(item)
                    }
                }
                activity!!.runOnUiThread {
                    try{
                    view.progressBar.setVisibility(View.GONE)
                    }
                    catch(exL : Exception){}
                }
            }
            catch (ex:Exception){
                activity!!.runOnUiThread {
                    try {
                        Toast.makeText(activity, "Connection Error...", Toast.LENGTH_SHORT).show()
                    }
                    catch(ex : Exception){

                    }
                }
            }

            val userx=userRepo.DirectRepoGetUserFromDatabase()
            activity!!.runOnUiThread {
                Toast.makeText(activity!!, userx.id.toString(), Toast.LENGTH_LONG).show()
            }
            val Test=itemRepo.getMyItemTst(userx.id)
            for (ite in Test){
                activity!!.runOnUiThread {
                    Toast.makeText(activity!!, ite.name, Toast.LENGTH_LONG).show()
                }
            }
            activity!!.runOnUiThread {
                itemViewModelM = ViewModelProviders.of(this@MyItems).get(ItemViewModel::class.java)
                val ticketsadapter =MyItemAdapter(this@MyItems.context!!)
                itemViewModelM.itemList.observe(this@MyItems, Observer {
                    ticketsadapter.setItemList(it)})
                recyclervTM=view.my_items_recy
                recyclervTM.layoutManager= LinearLayoutManager(this@MyItems.context)
                val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
                recyclervTM.setLayoutAnimation(controller)
                recyclervTM.adapter=ticketsadapter
            }
        }

        return view
    }


}
