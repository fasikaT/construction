package com.example.myapplication.repo

import androidx.lifecycle.LiveData
import com.example.myapplication.data.Item
import com.example.myapplication.data.ItemDao
import com.example.myapplication.network.ApiService
import retrofit2.Call

class ItemRepository(private val itemDao: ItemDao) {
    val apiServ:ApiService= ApiService.getInstance()

    fun getMyItemTst(id:Int):List<Item>{
        return itemDao.getMyItemtest(id)
    }
    fun delete(id:Int):Call<String>{
        return apiServ.delete(id)
    }
    fun getMyItemsFromDb(id:Int):LiveData<List<Item>>{
        return itemDao.getMyItemFromDb(id)
    }
    fun getOne(id:Int):LiveData<Item>{
        return itemDao.getOne(id)
    }
    fun addToNet(item: Item):Call<String>{
        return apiServ.addItem(item)
    }
    fun clear(){
        itemDao.ClearAllItems()
    }
    fun getMyItems(id:Int):Call<List<Item>>{
        return apiServ.fetchMyItems(id)
    }
    fun getItemList(): Call<List<Item>>{
        return apiServ.fetchAllItems()
    }
    fun readFromDb():LiveData<List<Item>>{
        return itemDao.getAllFromDb()
    }
    fun saveToDb(item: Item){
        itemDao.save(item)
    }
}