package com.et.cinemax.Repo

import androidx.lifecycle.LiveData
import com.example.myapplication.data.User
import com.example.myapplication.data.UserDao
import com.example.myapplication.network.ApiService
import retrofit2.Call

class UserRepository(private val userDao: UserDao) {
    val apiServ:ApiService= ApiService.getInstance()

    fun getInfoFromNet(id:Int):Call<User>{
        return apiServ.getInfoFromNet(id)
    }
    fun Save(user: User):Long
    {
        return userDao.save(user)
    }
    fun getUserFromDatabase(): LiveData<User> {
        return userDao.getUserFromDb()
    }
    fun DirectRepoGetUserFromDatabase():User {
        return userDao.DirectgetUserFromDb()
    }
    fun login(user:User): Call<String> {
        return apiServ.login(user)
    }
    fun register(user: User):Call<String>{
        return apiServ.register(user)
    }
    fun DeleteAll(){
        userDao.ClearAllUser()
    }
    /*
    fun getUserInfoFromNet(phone:String):Call<User>{
        return apiServ.GetInfoByPhone(phone)
    }
    fun getUserInfoFromNet(id:Int):Call<User>{
        return apiServ.GetInfoById(id)
    }
    fun DeleteAll(){
        userDao.ClearAllUser()
    }

    suspend fun RequestAuth(user:User): Call<Int> {
        return apiServ.RequestAuth(user)
    }

    fun AddBalance(user: User):Call<String>{
        return apiServ.AddBalance(user)
    }*/
}