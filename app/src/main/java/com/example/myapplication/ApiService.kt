package com.example.myapplication
import com.example.myapplication.model.User
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @GET("categories")
    fun fetchAllCategories(): Call<ArrayList<Category>>

    @POST("users/login")
    @FormUrlEncoded
    fun login(@Field("username") username: String, @Field("password") password: String): Call<User>

    @POST("users")
    @FormUrlEncoded
    fun register(@Field("username") username: String, @Field("password") password: String, @Field("email") email: String, @Field("fullname") fullname: String, @Field("phone") phone: String, @Field("userType") userType: String): Call<User>
}