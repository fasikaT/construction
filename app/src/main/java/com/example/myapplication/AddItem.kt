package com.example.myapplication


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.et.cinemax.Repo.UserRepository
import com.example.myapplication.data.Item
import com.example.myapplication.data.database
import com.example.myapplication.repo.ItemRepository
import kotlinx.android.synthetic.main.fragment_add_item.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AddItem : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_add_item, container, false)
        view.add_item_btn.setOnClickListener {
            var type=view.type_input.text.toString()
            var name=view.name_input.text.toString()
            var desc=view.desc_input.text.toString()
            var price=view.price_input.text.toString()

            GlobalScope.launch {
            val userDao= database.getDatabase(activity!!).userDao()
            val itemDao = database.getDatabase(activity!!).itemDao()
            val userRepo= UserRepository(userDao)
            val user=userRepo.DirectRepoGetUserFromDatabase()
            val itemRepo = ItemRepository(itemDao)
            val item=Item(0,type,price.toInt() ,desc,name,user.id)
            val addItem = itemRepo.addToNet(item)
            try {
                    val addResponse=addItem.execute().body()
                    if (addResponse=="1"){
                        activity!!.runOnUiThread {
                            Toast.makeText(activity,"Successfuly Added Item + ${user.id.toString()}",Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        activity!!.runOnUiThread {
                            Toast.makeText(activity,"Error...",Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            catch (ex:Exception){
                activity!!.runOnUiThread {
                    Toast.makeText(activity,"Connection Error...",Toast.LENGTH_SHORT).show()
                }
                }
            }
        }
        return view
    }


}
