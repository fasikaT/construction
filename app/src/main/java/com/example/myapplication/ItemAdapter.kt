package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.data.Item
import kotlinx.android.synthetic.main.one_item_view.view.*

class ItemAdapter(val context: Context): RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    private var tickets= emptyList<Item>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.ItemViewHolder {
        val inflater= LayoutInflater.from(parent.context)
        val recyclerViewItem=inflater.inflate(R.layout.one_item_view,parent,false)
        return ItemAdapter.ItemViewHolder(recyclerViewItem)
    }
    fun setItemList(tick : List<Item>){
        this.tickets=tick
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return tickets.size
    }

    override fun onBindViewHolder(holder:ItemAdapter.ItemViewHolder, position: Int) {
        val tick=tickets[position]
        holder.itemView.itemid.text=tick.id.toString()
        holder.itemView.itemid.setVisibility(View.GONE)
        holder.itemView.one_item_nameG.text=tick.name
        holder.itemView.one_item_typeG.text=tick.type
        holder.itemView.one_item_priceG.text=tick.price.toString()+" ETB"
    }
    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}